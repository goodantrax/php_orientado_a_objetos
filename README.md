# Ejercicios del curso PHP Orientado a Objetos
## Descripción

En este curso se abordan los conceptos de programación orientada a objetos para el lenguaje de programación PHP.

## Autor

* Bermejo López Axel Nahir

### Contacto

axel_berm13@hotmail.com

## Instructores

* Daniel Barajas González.

### Contacto

ldanielbg@comunidad.unam.mx